#!/usr/bin/env bash

PROJECT_ID="${1}"
VERSION="${2}"
TOKEN="${3}"

URL="https://gitlab.com/api/v4/projects/${PROJECT_ID}/trigger/pipeline"
OUTPUT_FILE=$(mktemp -t curl.output)

echo "Sending request to ${URL}"

STATUS=$(curl \
  --silent \
  --output "${OUTPUT_FILE}" \
  --write-out "%{http_code}" \
  --request POST \
  --form "ref=master" \
  --form "variables[VERSION]=${VERSION}" \
  --form "token=${TOKEN}" \
  "${URL}"
)

echo ${STATUS}
cat "${OUTPUT_FILE}"

rm -r ${OUTPUT_FILE}
